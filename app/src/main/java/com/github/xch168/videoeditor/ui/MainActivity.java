package com.github.xch168.videoeditor.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.xch168.ffmpeg_cmd.FFmpegCmd;
import com.github.xch168.videoeditor.R;
import com.github.xch168.videoeditor.util.AppUtil;

public class MainActivity extends BaseActivity {
    private static final String TAG = "MainActivity";
    public static final int TYPE_VIDEO_CLIP = 0;
    public static final int TYPE_VIDEO_COVER = 1;
    public static final int TYPE_VIDEO_MERGE = 2;
    public static final int TYPE_VIDEO_WATERMARK = 3;
    public static final int TYPE_VIDEO_CLIP_COMPOSE = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initVersionView();

        findViewById(R.id.tv_title).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String rtsp = "rtsp://admin:eboy123456@10.1.80.240:554/h265/ch01/sub/av_stream";;
                        String rtmp = "rtmp://10.1.80.126:1935/live/test_yin";
                        int result = FFmpegCmd.startRtmp(rtsp, rtmp);
                        Log.i(TAG, "onClick: start rtmp result=" + result);
                    }
                });
                thread.start();
            }
        });
    }

    private void initVersionView() {
        TextView versionView = findViewById(R.id.tv_version);
        versionView.setText("v" + AppUtil.getVersionName());
    }

    public void videoClip(View view) {
        VideoListActivity.open(this, TYPE_VIDEO_CLIP);
    }

    public void toChooseVideoCover(View view)
    {
        VideoListActivity.open(this, TYPE_VIDEO_COVER);
    }

    public void videoMerge(View view)
    {
        VideoChooseActivity.open(this);
    }

    public void videoWatermark(View view) {
        VideoListActivity.open(this, TYPE_VIDEO_WATERMARK);
    }

    public void videoClipAndCompose(View view) {
        VideoListActivity.open(this, TYPE_VIDEO_CLIP_COMPOSE);
    }
}
